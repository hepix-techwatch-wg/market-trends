# Semiconductor market trends

## Worldwide semiconductor revenue
**Last update: Sep, 2019**

The Semiconductor Industry Association (SIA) today announced worldwide sales of semiconductors were $33.4 billion in July 2019, 1.7 percent more than the June 2019 total of $32.8 billion, but 15.5 percent less than the July 2018 total of $39.5 billion. Monthly sales are compiled by the World Semiconductor Trade Statistics (WSTS) organization and represent a three-month moving average. SIA represents U.S. leadership in semiconductor manufacturing, design, and research.

“While global semiconductor sales in July were once again down on a year-to-year basis, month-to-month sales were up slightly,” said John Neuffer, SIA president and CEO. “Month-to-month sales increased modestly across most regional markets in July, with Asia Pacific and the Americas posting the largest gains, although sales into the Americas remained down on a year-to-year basis.”

[Source](https://www.semiconductors.org/global-semiconductor-sales-decrease-15-5-percent-year-to-year-in-july/)  

![Market Trends](images/Jul19-GSR-graph.png)  

[Original link](https://www.semiconductors.org/wp-content/uploads/2019/09/July-2019-GSR-table-and-graph-for-press-release.pdf)  
![Cached in repo](pdfs/January-2019-GSR-table-and-graph-for-press-release.pdf)

---
update: Jan, 2019

The Semiconductor Industry Association (SIA), representing U.S. leadership in semiconductor manufacturing, design, and research, today announced worldwide sales of semiconductors reached $35.5 billion for the month of January 2019, a decrease of 5.7 percent from the January 2018 total of $37.6 billion and 7.2 percent less than the December 2018 total of $38.2 billion. 

“Global semiconductor sales got off to a slow start in 2019, as year-to-year sales decreased in January for the first time since July 2016 and month-to-month sales were down across all major product categories and regional markets,” said John Neuffer, SIA president and CEO. “Following record sales over the last three years, reaching $469 billion in 2018, it seems clear the global market is experiencing a period of slower sales. The long-term outlook remains promising, however, due to the ever-increasing semiconductor content in a range of consumer products and future growth drivers such as artificial intelligence, virtual reality, the Internet of Things, and 5G and next-generation communications networks.

[Source](https://www.semiconductors.org/global-semiconductor-sales-down-5-7-percent-year-to-year-in-january/)  

![Market Trends](images/Jan19-GSR-graph.png)  

[Original link](https://www.semiconductors.org/wp-content/uploads/2019/03/January-2019-GSR-table-and-graph-for-press-release.pdf)  
![Cached in repo](pdfs/July-2019-GSR-table-and-graph-for-press-release.pdf)   
___
update: Dec, 2018

The Semiconductor Industry Association (SIA), representing U.S. leadership in semiconductor manufacturing, design, and research, today announced the global semiconductor industry posted sales of $468.8 billion in 2018, the industry's highest-ever annual total and an increase of 13.7 percent compared to the 2017 total. Global sales for the month of December 2018 reached $38.2 billion, a slight increase of 0.6 percent over the December 2017 total, but down 7.0 percent compared to the total from November 2018. Fourth-quarter sales of $114.7 billion were 0.6 percent higher than the total from the fourth quarter of 2017, but 8.2 percent less than the third quarter of 2018. All monthly sales numbers are compiled by the World Semiconductor Trade Statistics (WSTS) organization and represent a three-month moving average.

"Global demand for semiconductors reached a new high in 2018, with annual sales hitting a high-water mark and total units shipped topping 1 trillion for the first time," said John Neuffer, SIA president and CEO. "Market growth slowed during the second half of 2018, but the long-term outlook remains strong. Semiconductors continue to make the world around us smarter and more connected, and a range of budding technologies – artificial intelligence, virtual reality, the Internet of Things, among many others – hold tremendous promise for future growth."

Several semiconductor product segments stood out in 2018. Memory was the largest semiconductor category by sales with $158.0 billion in 2018, and the fastest growing, with sales increasing 27.4 percent. Within the memory category, sales of DRAM products increased 36.4 percent and sales of NAND flash products increased 14.8 percent. Logic ($109.3 billion) and micro-ICs ($67.2 billion) – a category that includes microprocessors – rounded out the top three product categories in terms of total sales. Other fast-growing product categories in 2018 included power transistors (14.4 percent growth/total sales of $14.4 billion) and analog products (10.8 percent growth/total sales of $58.8 billion). Even without sales of memory products, sales of all other products combined increased by nearly 8 percent in 2018.   

[Source](https://www.prnewswire.com/news-releases/global-semiconductor-sales-increase-13-7-percent-to-468-8-billion-in-2018--300788522.html)

![Market Trend Image](images/Dec18-GSR-graph.png)

[Original link](https://www.semiconductors.org/wp-content/uploads/2019/02/December-2018-GSR-table-and-graph-for-press-release.pdf)  
![Cached in repo](pdfs/December-2018-GSR-table-and-graph-for-press-release.pdf)  
___
update: Nov, 2018

The Semiconductor Industry Association (SIA), representing U.S. leadership in semiconductor manufacturing, design, and research, today announced worldwide sales of semiconductors reached $41.4 billion for the month of November 2018, an increase of 9.8 percent from the November 2017 total of $37.7 billion and 1.1 percent less than the October 2018 total of $41.8 billion.

![Market Trend Table](images/November-2018-GSR-table.png)
![Market Trend Image](images/November-2018-GSR-graph.png)

[Original link](https://www.semiconductors.org/global-semiconductor-sales-up-9-8-percent-year-to-year-in-november)

## Semiconductor unit growth
**Last update: Jan. 24, 2019**

Semiconductor unit shipments climbed to 1,068.2 billion units in 2018 and are expected to climb to 1,142.6 billion in 2019, which equates to 7% growth.

![Semiconductor unit growth](images/SemiconductorUnit_Growth_24Jan19.png)  

The percentage split of total semiconductor shipments is forecast to remain heavily weighted towards O-S-D devices in 2019.  The semiconductor categories forecast to have the strongest unit growth rates in 2019 are those that are essential building-blocks for smartphones, automotive electronics systems and devices that are used in computing systems essential to deep learning applications.

![Semiconductor unit shipment](images/SemiconductorUnit_Shipment_24Jan19.png)  

[Original link](http://www.icinsights.com/data/articles/documents/1132.pdf)  
![Cached in repo](pdfs/Report_IC-Insight_24Jan19_SemiUnitShip.pdf)  


## Miscellaneous

![Cached: Semiconductor Trend by IHS Markit Q1 2018](pdfs/CMP718-1-IHS.pdf)   
[Original link](https://nccavs-usergroups.avs.org/wp-content/uploads/CMPUG2018/CMP718-1-IHS.pdf)   

![Cached: Semiconductor Trend by Semicon Europa Nov 2018](pdfs/SemiconEuropaNov18.pdf)   
[Original link](http://www1.semi.org/eu/sites/semi.org/files/events/presentations/01_Lara%20Chamness_SEMI_0.pdf)   


## Semiconductor market predictions from 2015
Updated: 2015

Interesting statistics:
- Fraction of semiconductor types
- IoT Semiconductor and Sensor Markets
- Foundry Market by Feature Dimension (constant, except low density semiconductors)

[Original link 1](http://www1.semi.org/en/node/57416)  
[Original link 2](http://www.semicontaiwan.org/en/sites/semicontaiwan.org/files/data16/docs/Topic%203-%20Handel%20Jones-0816.pdf)  
[Original link 3](https://www.statista.com/statistics/513880/worldwide-internet-of-things-semiconductor-and-sensor-market/)  

note: Content not cached since the last update is already four years ago