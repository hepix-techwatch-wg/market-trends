# Manufacturing Process Technology

Updated: Nov. 8th, 2018

Lithography technology necessary to achieve a given resolution, from ASML Investor Day 2018 presentation “Industry Roadmap and Technology Strategy”,  Martin van den Brink CTO ASML.

![Manufacturing Process Customers](images/ManufacturingProcessCustomers_8Nov18.png)
![Manufacturing Process Scaling](images/ManufacturingProcessScaling_8Nov18.png)
![Manufacturing Process Technology](images/ManufacturingProcessTechnology_8Nov18.png)  

[Cached](pdfs/asml_20181108_03_ASML_Investor_Day_2018_Industry_Roadmap_and_Technology_Strategy_MvdBrink.pdf)
[Original link](http://staticwww.asml.com/doclib/investor/investor_day/asml_20181108_03_ASML_Investor_Day_2018_Industry_Roadmap_and_Technology_Strategy_MvdBrink.pdf)