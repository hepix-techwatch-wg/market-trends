# Technolgy companies and markets by revenue and shipment

## Server market
Updated: Dec. 11th, 2018

According to the International Data Corporation (IDC) Worldwide Quarterly Server Tracker, vendor revenue in the worldwide server market increased 37.7%, year over year to $23.4 billion during the third quarter of 2018 (3Q18). Worldwide server shipments increased 18.3% year over year to 3.2 million units in 3Q18.

The overall server market continues to experience robust demand with 3Q18 marking the fifth consecutive quarter of double-digit revenue growth and its highest total revenue in a single quarter ever. Volume server revenue increased by 40.2% to $20.0 billion, while midrange server revenue grew 39.4% to $2.0 billion. High-end systems grew 6.9% to $1.3 billion.

![Server vendor share](images/idc-q3-2018-server-vendor.jpg)  
[Original link](https://www.idc.com/getdoc.jsp?containerId=prUS44532818)

___

Updated: Sep. 6th, 2018

ODM Direct sales are increasing at a rapid pace. A likely scenario being a substantial fraction of these sales are due to hyperscalers such as Amazon, Google, or Microsoft.

![Server revenue share](images/idc-q2-2018-server-revenue-units.jpg)  
![Server vendor share](images/idc-q2-2018-server-vendor.jpg)  

[Original link](https://www.nextplatform.com/2018/09/06/are-hyperscalers-and-clouds-builders-smashing-up-an-it-pangea)

## PC market
Updated: Jan. 10, 2019

Preliminary results for the fourth quarter of 2018 (4Q18) showed shipments of traditional PCs (desktop, notebook, and workstation) totaled just over 68.1 million units, marking a decline of 3.7% in year-on-year terms, according to the International Data Corporation (IDC) Worldwide Quarterly Personal Computing Device Tracker. The results slightly outperformed the forecast, which called for a decline of 4.7%, but also produced the largest year-on-year decline since the third quarter of 2016 (3Q16) and capped the full year at a nearly flat rate of -0.4%.
![Top5 PCD](images/IDC_Top5_PCD_Q42018.jpg)


[Original link 1](https://www.idc.com/getdoc.jsp?containerId=prUS44670319)  
[Original link 2](https://www.gartner.com/en/newsroom/press-releases/2019-01-10-gartner-says-worldwide-pc-shipments-declined-4-3-perc)  