# Embedded computing share
Updated: Feb. 1st, 2018

Breakdown of CPU sales by market from IC Insights shows the relative size of the different ecosystems in figure "MPU sales by application" in the report below.  

The same report shows sales of CPUs into the mobile market is a grown share of the overall market in figure "Shifting microprocessor" in the report below.  

![Report](pdfs/Report_IC-Insight_08Feb18_Embedded.pdf)   
[Original link](http://www.icinsights.com/data/articles/documents/1043.pdf)