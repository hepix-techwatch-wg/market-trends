[Digital Trends 2019](https://wearesocial.com/blog/2019/01/digital-2019-global-internet-use-accelerates)

![Annual Smart Population Growth 2019](images/AnnualGrowthSmartPopulation.png)

![Mobile Connections Growth 2019](images/MobileConnectionsGrowth.png)

![More digital trends](pdfs/DigitalTrends2019_Reduced.pdf)