# Semiconductor Fabrication Technology

## Global Wafer Capacity
Updated: Feb. 14, 2019

Taiwan led all regions/countries in wafer capacity with 21.8% share, a slight increase from 21.3% in 2017.
TSMC held 67% of Taiwan’s capacity while Samsung and SK Hynix represented 94% of the installed IC wafer capacity in South Korea at the end of 2018.

![Wafer Capacity end of 2018](images/WaferCapacity_EndDec18.png)   
[Cached](pdfs/Report_IC-Insight_14Feb19_WaferCapacity.pdf)  
[Original Link](http://www.icinsights.com/data/articles/documents/1141.pdf)  

[List of Semiconductor Fabrication Plants](https://en.wikipedia.org/wiki/List_of_semiconductor_fabrication_plants)  

___   

Updated: Feb. 08, 2018

New manufacturing lines are expected to boost industry capacity 8% in both 2018 and 2019. From 2017-2022, annual growth in IC industry capacity is forecast to average 6.0% compared to 4.8% average growth from 2012-2017.

![Wafer Capacity end of 2017](images/WaferCapacity_EndDec17.png)  
[Cached](pdfs/Report_IC-Insight_08Feb18_WaferCapacity.pdf)  
[Original Link](http://www.icinsights.com/data/articles/documents/1044.pdf)  

___