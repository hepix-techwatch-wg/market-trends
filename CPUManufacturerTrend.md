# Intel Vs AMD CPUs market share


## Passmark based overall share
**All laptop, desktop and server CPUs that are used in passmark benchmarking**  
Updated: Jan. 31, 2019

[Intel Vs AMD cpu market share Realtime Update](https://www.cpubenchmark.net/market_share.html)

![Intel Vs AMD Jan 2019](images/Intel_Vs_AMD_Market_Share_Jan_2019.png)


## Server only revenue share
Updated: Q2 2018

Intel's server CPU share is estimated to have declined to 98.7 per cent vs 99 per cent in the prior period and 99.5 per cent a year ago.
AMD's server CPU share increased to 1.3 per cent in 2Q18, up from 1 per cent in the prior period and 0.5 per cent a year ago.
AMD's total server units were up 181 per cent year-over-year and 40.5 per cent sequentially. Mercury estimates Epyc revenue was $57.66m in the second 2018 quarter vs $36m in the prior quarter.
Rakers said AMD believes it can grow its current server market share position to mid-single digits by the end of calendar 2018.
Achieving a 5 per cent share in just two quarters from the current 1.3 per cent level and 0.4 per cent/quarter growth rate looks unachievable.
AMD could reach 2.1 per cent by the end of the year at its current share growth rate.

![x86 Server Revenue Share Q2 2018](images/amd_server_cpu_revnue_share_2q2018.jpg)

[Original Link](https://www.theregister.co.uk/2018/08/06/amd_epyc_intel_xeon_x86_server_revenue_share/)