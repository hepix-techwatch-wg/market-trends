## 3D NAND flash trend
Updated: Early 2019

***Not sure if this file can be shared***
***It required an account creation for download***
***and was not publicly accessible***

The statistic shows a forecast for the distribution of demand for NAND memory bits worldwide in 2019, broken down by device segment. In 2019, 35 percent of total NAND memory capacity will go towards smartphones. NAND flash memory was originally developed by Toshiba in the early 1980s. It allows greater storage density and lower cost per bit than the alternative NOR flash, but is more useful as a secondary storage device than as primary storage, as it does not support a random-access external address bus. 

![NAND Statistic 2019](images/statistic_id553338_global-nand-memory-bit-market-share-2019-by-application.png)

[Original Link](https://www.statista.com/statistics/553338/worldwide-nand-memory-bit-demand-distribution/)  